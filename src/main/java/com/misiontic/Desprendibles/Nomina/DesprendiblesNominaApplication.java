package com.misiontic.Desprendibles.Nomina;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesprendiblesNominaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesprendiblesNominaApplication.class, args);
	}

}
