package com.misiontic.Desprendibles.Nomina.repositories;

import java.util.List;

import com.misiontic.Desprendibles.Nomina.models.DesprendiblesNomina;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface DesprendibleNominaRepository extends MongoRepository<DesprendiblesNomina, String> {
    List<DesprendiblesNomina> findByUsername (String username); 
}
