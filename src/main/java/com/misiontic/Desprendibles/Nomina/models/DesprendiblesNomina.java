package com.misiontic.Desprendibles.Nomina.models;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class DesprendiblesNomina {
    
    @Id
    private String id;

    private String username;
    private String nombre1;
    private String nombre2;
    private String apellido1;
    private String apellido2;
    private String documento;
    private double credito;
    private double salario;

    private Date fecha;
    private double salud;
    private double pension;
    private double devengado;
    private double deduccion;
    private double valorTotal;

    public DesprendiblesNomina() {
    }

    public DesprendiblesNomina(String id) {
        this.id = id;
    }
    
    public DesprendiblesNomina(String id, String username, String nombre1, String nombre2, String apellido1,
            String apellido2, String documento, double credito, double salario, Date fecha, double salud,
            double pension, double devengado, double deduccion, double valorTotal) {
        this.id = id;
        this.username = username;
        this.nombre1 = nombre1;
        this.nombre2 = nombre2;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.documento = documento;
        this.credito = credito;
        this.salario = salario;
        this.fecha = fecha;
        this.salud = salud;
        this.pension = pension;
        this.devengado = devengado;
        this.deduccion = deduccion;
        this.valorTotal = valorTotal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public double getCredito() {
        return credito;
    }

    public void setCredito(double credito) {
        this.credito = credito;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getSalud() {
        return salud;
    }

    public void setSalud(double salud) {
        this.salud = salud;
    }

    public double getPension() {
        return pension;
    }

    public void setPension(double pension) {
        this.pension = pension;
    }

    public double getDevengado() {
        return devengado;
    }

    public void setDevengado(double devengado) {
        this.devengado = devengado;
    }

    public double getDeduccion() {
        return deduccion;
    }

    public void setDeduccion(double deduccion) {
        this.deduccion = deduccion;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }
    
    
}
