package com.misiontic.Desprendibles.Nomina.exceptions;

public class DespNominaException extends RuntimeException {
    public DespNominaException(String message) {
        super(message);
        }
}
