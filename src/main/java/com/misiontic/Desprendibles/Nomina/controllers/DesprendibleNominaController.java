package com.misiontic.Desprendibles.Nomina.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.misiontic.Desprendibles.Nomina.exceptions.DespNominaException;
import com.misiontic.Desprendibles.Nomina.models.DesprendiblesNomina;
import com.misiontic.Desprendibles.Nomina.repositories.DesprendibleNominaRepository;

import org.springframework.web.bind.annotation.*;


@RestController
public class DesprendibleNominaController {

    public double auxTrasporte = 106454;
    public double salarioMinimo = 908526;


    private final DesprendibleNominaRepository desprendibleNominaRepository;

    public DesprendibleNominaController(DesprendibleNominaRepository desprendibleNominaRepository) {
        this.desprendibleNominaRepository = desprendibleNominaRepository;
    }

    @PostMapping("/DN")
    public DesprendiblesNomina newDesprendiblesNomina(@RequestBody DesprendiblesNomina dNomina) {
        
        if (
            dNomina.getUsername() == null ||
            dNomina.getNombre1() == null ||
            dNomina.getNombre2() == null ||
            dNomina.getApellido1() == null ||
            dNomina.getApellido2() == null ||
            dNomina.getDocumento() == null ||
            dNomina.getSalario() == 0
            ) {
                throw new DespNominaException("Hay un campo null");
        }

        if (dNomina.getSalario() > (salarioMinimo * 2)) {
            dNomina.setDevengado(dNomina.getSalario());
        } else {
            dNomina.setDevengado(dNomina.getSalario() + auxTrasporte);
        }

        dNomina.setFecha(new Date());
        dNomina.setPension(dNomina.getSalario() * 0.04);
        dNomina.setSalud(dNomina.getSalario() * 0.04);
        dNomina.setDeduccion(dNomina.getPension() + dNomina.getSalud() + dNomina.getCredito());
        dNomina.setValorTotal(dNomina.getSalario() - dNomina.getDeduccion());

        return desprendibleNominaRepository.save(dNomina);
    }

    @GetMapping(value="/DN/{username}")
    List<DesprendiblesNomina>  getDesprendiblesNomina(@PathVariable String username) {
        System.out.println(username);
        List<DesprendiblesNomina> dsNomina = desprendibleNominaRepository.findByUsername(username);
        return dsNomina;
    }

    @PutMapping("/DN/{id}")
    public DesprendiblesNomina updateDesprendiblesNomina(@RequestBody DesprendiblesNomina dNomina, @PathVariable String id) {
        
        Optional<DesprendiblesNomina> dn = desprendibleNominaRepository.findById(id);

        if (dn.isPresent()) {
            DesprendiblesNomina _dn = dn.get();
            if (
                dNomina.getSalario() == 0
                ) {
                    throw new DespNominaException("Hay un campo null");
            }

            if (_dn.getSalario() > (salarioMinimo * 2)) {
                _dn.setDevengado(dNomina.getSalario());
            } else {
                _dn.setDevengado(dNomina.getSalario() + auxTrasporte);
            }

            _dn.setFecha(new Date());
            _dn.setPension(dNomina.getSalario() * 0.04);
            _dn.setSalud(dNomina.getSalario() * 0.04);
            if (dNomina.getCredito() > 0) {
                _dn.setDeduccion(_dn.getPension() + _dn.getSalud() + dNomina.getCredito());
            } else {
                _dn.setDeduccion(_dn.getPension() + _dn.getSalud());
            }
            
            _dn.setValorTotal(dNomina.getSalario() - dNomina.getDeduccion());
            return desprendibleNominaRepository.save(_dn);
        } else {
            throw new DespNominaException("No se encontro nomina");
        }
    
    }
    
    @DeleteMapping("/DN/{id}")
    public String deleteDesprendiblesNomina(@PathVariable String id) {
        desprendibleNominaRepository.delete(new DesprendiblesNomina(id));
        return "Eliminado correctamente";
    }
}
